class SessionController < MainController
	
	def new
	end
	
	def create
		user = User.find_by_email(params[:session][:email])
		render :json => { :errors => ["Email not found"] }, :status => 422 if user.nil?
		if user && user.authenticate(params[:session][:password])
			user.generate_session_token
			user.save
			render :json => user
		else
			render :json => { :errors => ["Incorrect password"] }, :status => 422
		end
	end

	def delete
	end

end