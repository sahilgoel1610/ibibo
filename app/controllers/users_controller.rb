class UsersController < MainController

	# before_action :doorkeeper_authorize!, :only => [:show, :update]

	def new
		
	end

	def create
		user = User.new(:email => params["user"]["email"], :password => params["user"]["password"], :name => params["user"]["name"])
		if user.save
			render :json => user
		else
			render :json => { :errors => user.errors.full_messages }, :status => 422
		end
	end


	def show
		redirect_to :new
	end

	def confirm_email
		redirect_to :show if User.set_email_verification(params["user"]["email"],params["user"]["code"])
	end
end