class User < ActiveRecord::Base
	has_secure_password
	# EMAIL_REGEX = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i
	validates :name, :presence => true, :length => { :in => 3..20 }
	validates :email, :presence => true, :uniqueness => true, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, on: :create }
	validates_length_of :password, :in => 6..20, :on => :create

	before_save :encrypt_password
	before_create :set_secret_code, :generate_session_token
	after_create :send_email_confirmation_link

	def encrypt_password
		# if self.password.present?
		# 	self.password = Digest::SHA1.hexdigest(self.password)
		# end
	end

	def set_secret_code
		self.code = (0...8).map { (65 + rand(26)).chr }.join
	end

	def send_email_confirmation_link
		UserMailer.confirmation_email(self).deliver
	end

	def self.set_email_verification(email,code)
		return false if email.nil? or code.nil?
		user = User.find_by(:email => email)
		user.update(:code => nil, :email_verification => true) if user.present? && user.code == code
	end

	 def generate_session_token
    self.token = loop do
      random_token = SecureRandom.urlsafe_base64(nil, false)
      break random_token unless User.exists?(token: random_token)
    end
  end

end