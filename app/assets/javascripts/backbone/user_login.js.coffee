#= require_self
#= require_tree ./templates
#= require_tree ./models
#= require_tree ./views
#= require_tree ./routers

window.UserLogin =
	Models: {}
	Collections: {}
	Routers: {}
	Views: {}
	init: ->
		user_router = new UserLogin.Routers.UsersRouter();
		new UserLogin.Routers.SessionRouter();
		Backbone.history.start();

$(document).ready ->
	UserLogin.init()
