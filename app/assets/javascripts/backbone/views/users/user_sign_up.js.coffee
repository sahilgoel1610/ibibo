class UserLogin.Views.UserSignUp extends Backbone.View
	
	template: JST["backbone/templates/users/sign_up"]

	initialize: ->
		@model = new @collection.model()
		@model.bind("change:errors", () =>
			this.render()
		)

	render: ->
		$(@el).html(@template(@model))
		return this

	events:
		'submit #sign_in' : 'sign_in_user'



	sign_in_user: ->
		event.preventDefault()
		attributes = email: $('#email').val(), password: $('#password').val(), name: $('#username').val()
		@model.save attributes, 
			wait: true,
			success: (@model) ->
				window.location.hash = "/#{@model.id}"
			error: (@model, jqXHR) ->
				@model.set({errors: $.parseJSON(jqXHR.responseText)})
		

