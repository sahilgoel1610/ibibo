class UserLogin.Views.UserShow extends Backbone.View
	
	template: JST["backbone/templates/users/show"]

	render: ->
		$(@el).html(@template(@model))

		return this