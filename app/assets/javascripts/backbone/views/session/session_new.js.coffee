class UserLogin.Views.SessionNew extends Backbone.View
	
	template: JST["backbone/templates/session/new"]

	initialize: ->
		@model = new UserLogin.Models.Session
		@model.bind("change:errors", () =>
			this.render()
		)

	render: ->
		$(@el).html(@template(@model))
		return this

	events:
		'submit #sign_in' : 'sign_in_user'


	sign_in_user: ->
		event.preventDefault()
		attributes = email: $('#email').val(), password: $('#password').val()
		@model.save attributes, 
			wait: true,
			success: (@model) ->
				sessionStorage.token = @model.get("token")
				window.location.hash = "/#{@model.id}"
			error: (@model, jqXHR) ->
				@model.set({errors: $.parseJSON(jqXHR.responseText)})
		

