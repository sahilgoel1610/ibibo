class UserLogin.Models.Session extends Backbone.Model
	paramRoot: 'session'

	urlRoot: '/session'

	defaults: ->
		'email' : 'na'
		'password': 'na'
		'auth_token': 'na'