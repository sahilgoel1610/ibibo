class UserLogin.Models.User extends Backbone.Model
	paramRoot: 'user'

	urlRoot: '/users'

	defaults: ->
		'email' : 'na'
		'password': 'na'
		'token' : 'na'
	
class UserLogin.Collections.UsersCollection extends Backbone.Collection
  model: UserLogin.Models.User
  url: '/users'
