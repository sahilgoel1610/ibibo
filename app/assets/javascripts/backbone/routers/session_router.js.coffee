class UserLogin.Routers.SessionRouter extends Backbone.Router
	initialize: () ->

	routes:
		"" : "new",
		"logout" : "logout"

	new: ->
		view = new UserLogin.Views.SessionNew()
		$('#container').html(view.render().el)

	logout: ->
		sessionStorage.clear()
		window.location.hash = "/"

