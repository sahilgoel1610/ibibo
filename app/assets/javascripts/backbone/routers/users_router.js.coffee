class UserLogin.Routers.UsersRouter extends Backbone.Router
	initialize: () ->
		@users = new UserLogin.Collections.UsersCollection()
	
	routes:
		"sign_up" : "create"
		":id"      : "show"

	create: ->
		view = new UserLogin.Views.UserSignUp(collection: @users)
		$('#container').html(view.render().el)

	show:(id) ->

		user = new @users.model( {'id': id, 'token': sessionStorage.token})
		view = new UserLogin.Views.UserShow(model: user)
		$('#container').html(view.render().el)