class ApplicationMailer < ActionMailer::Base
	# include SendGrid

	# sendgrid_category :use_subject_lines
  default from: "from@example.com"
  layout 'mailer'
end
