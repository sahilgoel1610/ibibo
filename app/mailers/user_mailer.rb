class UserMailer < ApplicationMailer
	
	def confirmation_email(user)
		# sendgrid_category "Confirmation Email"
		@user = user
    mail :to => user.email, :subject => "Welcome #{user.name}! Please confirm your email"		
	end
end
