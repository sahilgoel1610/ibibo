# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This is a small app that contains APIs for creating a user and creating a session running on rails. These APIs are consumed by backbone frontend app. The frontend app is written in coffee script which then converts to JS and make use of the backboneJS library to run the app as an MVC framework.

Backbone js contains a user model, which consumes user API and a session model which hits the "create" of session API to fetch a authentication token from the backend. This authentication is saved in sessionStorage of the client. 

This token should be sent back to the server incase of 'show' and 'edit' request of  User resource. The user would then be queried against this token and grant access to the show and edit rights.[incomplete]


The delete API of the session(logout) would destroy the authentication token saved in the user database and on successful deletion would also delete the token from client's sessionStorage.[incomplete]


 

### How do I get set up? ###

* fork the repo into your local machine.
* install bundler by running the command "gem install bundler"
* run "bundle" to load all the gem and dependencies
* change the database configurations by going to database.yml to set up the database.
* run rake db:migrate to create the database